package ru.t1.fpavlov.tm.comparator;

import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.api.model.IHasCreated;

import java.util.Comparator;

/**
 * Created by fpavlov on 26.11.2021.
 */
public enum CreatedComparator implements Comparator<IHasCreated> {

    INSTANCE;

    @Override
    public int compare(
            @Nullable final IHasCreated item1,
            @Nullable final IHasCreated item2
    ) {
        if (item1 == null || item2 == null) return 0;
        if (item1.getCreated() == null || item2.getCreated() == null) return 0;
        return item1.getCreated().compareTo(item2.getCreated());
    }

}
