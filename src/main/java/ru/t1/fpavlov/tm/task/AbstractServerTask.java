package ru.t1.fpavlov.tm.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.fpavlov.tm.component.Server;

/**
 * Created by fpavlov on 01.04.2022.
 */
public abstract class AbstractServerTask implements Runnable {

    @NotNull
    protected Server server;

    public AbstractServerTask(@NotNull final Server server) {
        this.server = server;
    }

}
