package ru.t1.fpavlov.tm.exception.entity;

import org.jetbrains.annotations.NotNull;

/**
 * Created by fpavlov on 06.12.2021.
 */
public final class TaskNotFoundException extends AbstractEntityNotFoundException {

    @NotNull
    public TaskNotFoundException() {
        super("Error! The task wasn't found. Correct parameters of your request");
    }

}
