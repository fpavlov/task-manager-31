package ru.t1.fpavlov.tm.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.fpavlov.tm.component.Server;

import java.net.Socket;

/**
 * Created by fpavlov on 01.04.2022.
 */
public abstract class AbstractServerSocketTask extends AbstractServerTask {

    @NotNull
    protected final Socket socket;

    public AbstractServerSocketTask(
            @NotNull final Server server,
            @NotNull final Socket socket
    ) {
        super(server);
        this.socket = socket;
    }

}
