package ru.t1.fpavlov.tm.api.component;

import org.jetbrains.annotations.NotNull;

/**
 * Created by fpavlov on 29.01.2022.
 */
public interface ISaltProvider {

    @NotNull
    Integer getPasswordIteration();

    @NotNull
    String getPasswordSecret();

}
