package ru.t1.fpavlov.tm.api.endpoint;

import ru.t1.fpavlov.tm.dto.request.AbstractRequest;
import ru.t1.fpavlov.tm.dto.response.AbstractResponse;

/**
 * Created by fpavlov on 01.04.2022.
 */
@FunctionalInterface
public interface Operation<RQ extends AbstractRequest, RS extends AbstractResponse> {

    RS execute(RQ request);

}
