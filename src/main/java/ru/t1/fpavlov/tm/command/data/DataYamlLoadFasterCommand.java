package ru.t1.fpavlov.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.fpavlov.tm.dto.Domain;

import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Created by fpavlov on 01.02.2022.
 */
public final class DataYamlLoadFasterCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-yaml-faster";

    @NotNull
    public static final String DESCRIPTION = "Load data from yaml file";

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @SneakyThrows
    public void execute() {
        @NotNull final byte bytes[] = Files.readAllBytes(Paths.get(FILE_XML));
        @NotNull final String yaml = new String(bytes);
        @NotNull final ObjectMapper objectMapper = new YAMLMapper();
        @NotNull Domain domain = objectMapper.readValue(yaml, Domain.class);
        this.setDomain(domain);
    }

}
