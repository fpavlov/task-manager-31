package ru.t1.fpavlov.tm.exception.field;

import org.jetbrains.annotations.NotNull;

/**
 * Created by fpavlov on 06.12.2021.
 */
public final class IndexIncorrectException extends AbstractFieldException {

    @NotNull
    public IndexIncorrectException() {
        super("Error! Index is incorrect");
    }

}
