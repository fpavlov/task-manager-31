package ru.t1.fpavlov.tm.client;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.fpavlov.tm.api.endpoint.ISystemEndpoint;
import ru.t1.fpavlov.tm.dto.request.ServerAboutRequest;
import ru.t1.fpavlov.tm.dto.request.ServerVersionRequest;
import ru.t1.fpavlov.tm.dto.response.ServerAboutResponse;
import ru.t1.fpavlov.tm.dto.response.ServerVersionResponse;

/**
 * Created by fpavlov on 01.04.2022.
 */
public final class SystemEndpointClient extends AbstractEndpoint implements ISystemEndpoint {

    @NotNull
    @Override
    @SneakyThrows
    public ServerAboutResponse getAbout(@NotNull final ServerAboutRequest request) {
        return (ServerAboutResponse) this.call(request);
    }

    @NotNull
    @Override
    @SneakyThrows
    public ServerVersionResponse getVersion(@NotNull final ServerVersionRequest request) {
        return (ServerVersionResponse) this.call(request);
    }

    @SneakyThrows
    public static void main(String[] args) {
        final SystemEndpointClient client = new SystemEndpointClient();
        client.connect();
        final ServerAboutResponse aboutResponse = client.getAbout(new ServerAboutRequest());
        System.out.println(aboutResponse.getName());
        System.out.println(aboutResponse.getEmail());
        final ServerVersionResponse versionResponse = client.getVersion(new ServerVersionRequest());
        System.out.println(versionResponse);
        client.disconnect();
    }

}
