package ru.t1.fpavlov.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.t1.fpavlov.tm.command.data.AbstractDataCommand;
import ru.t1.fpavlov.tm.command.data.DataBackupLoadCommand;
import ru.t1.fpavlov.tm.command.data.DataBackupSaveCommand;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by fpavlov on 06.02.2022.
 */
public final class Backup {

    @NotNull
    private final ScheduledExecutorService es = Executors.newSingleThreadScheduledExecutor();

    @NotNull
    private final Bootstrap bootstrap;

    public Backup(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void start() {
        this.load();
        this.es.scheduleWithFixedDelay(this::save, 0, 3, TimeUnit.SECONDS);
    }

    public void stop() {
        this.es.shutdown();
    }

    public void save() {
        this.bootstrap.listenerCommand(DataBackupSaveCommand.NAME, false);
    }

    public void load() {
        if (!Files.exists(Paths.get(AbstractDataCommand.FILE_BACKUP))) return;
        bootstrap.listenerCommand(DataBackupLoadCommand.NAME, false);
    }

}
