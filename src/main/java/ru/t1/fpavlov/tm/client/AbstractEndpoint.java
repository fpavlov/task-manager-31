package ru.t1.fpavlov.tm.client;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.io.*;
import java.net.Socket;

/**
 * Created by fpavlov on 01.04.2022.
 */
@Getter
@Setter
public abstract class AbstractEndpoint {

    private String host = "localhost";

    private Integer port = 6060;

    private Socket socket;

    public AbstractEndpoint() {

    }

    public AbstractEndpoint(@NotNull final String host, @NotNull final Integer port) {
        this.host = host;
        this.port = port;
    }

    protected Object call(final Object data) throws IOException, ClassNotFoundException {
        getObjectOutputStream().writeObject(data);
        return getObjectInputStream().readObject();
    }

    private ObjectInputStream getObjectInputStream() throws IOException {
        return new ObjectInputStream(getInputStream());
    }

    private ObjectOutputStream getObjectOutputStream() throws IOException {
        return new ObjectOutputStream(getOutputStream());
    }

    private InputStream getInputStream() throws IOException {
        return this.socket.getInputStream();
    }

    private OutputStream getOutputStream() throws IOException {
        return this.socket.getOutputStream();
    }

    public void connect() throws IOException {
        this.socket = new Socket(this.host, this.port);
    }

    public void disconnect() throws IOException {
        this.socket.close();
    }

}
