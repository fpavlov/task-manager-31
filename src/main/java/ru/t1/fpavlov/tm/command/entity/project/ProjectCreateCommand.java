package ru.t1.fpavlov.tm.command.entity.project;

import org.jetbrains.annotations.NotNull;

/**
 * Created by fpavlov on 08.12.2021.
 */
public final class ProjectCreateCommand extends AbstractProjectCommand {

    @NotNull
    public static final String DESCRIPTION = "Create new project";

    @NotNull
    public static final String NAME = "project-create";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        @NotNull final String name = this.askEntityName();
        @NotNull final String description = this.askEntityDescription();
        @NotNull final String userId = this.getUserId();
        this.getProjectService().create(userId, name, description);
    }

}
