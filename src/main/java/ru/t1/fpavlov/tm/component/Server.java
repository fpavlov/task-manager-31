package ru.t1.fpavlov.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.api.endpoint.Operation;
import ru.t1.fpavlov.tm.api.service.IPropertyService;
import ru.t1.fpavlov.tm.dto.request.AbstractRequest;
import ru.t1.fpavlov.tm.dto.response.AbstractResponse;
import ru.t1.fpavlov.tm.task.AbstractServerTask;
import ru.t1.fpavlov.tm.task.ServerAcceptTask;

import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by fpavlov on 17.03.2022.
 */
public final class Server {

    @lombok.Getter
    @Nullable
    private ServerSocket serverSocket;

    @NotNull
    private final ExecutorService executorService = Executors.newCachedThreadPool();

    @NotNull
    private final Dispatcher dispatcher = new Dispatcher();

    @NotNull
    private final Bootstrap bootstrap;

    public Server(@NotNull final Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
    }

    public void submit(@NotNull final AbstractServerTask task) {
        this.executorService.submit(task);
    }

    @SneakyThrows
    public void start() {
        @NotNull final IPropertyService propertyService = this.bootstrap.getPropertyService();
        @NotNull final Integer port = propertyService.getServerPort();
        this.serverSocket = new ServerSocket(port);
        this.submit(new ServerAcceptTask(this));
    }

    public <RQ extends AbstractRequest, RS extends AbstractResponse> void registry(
            @NotNull final Class<RQ> reqClass,
            @NotNull final Operation<RQ, RS> operation) {
        this.dispatcher.regitry(reqClass, operation);
    }

    @NotNull
    public Object call(@NotNull final AbstractRequest request) {
        return this.dispatcher.call(request);
    }

    @SneakyThrows
    public void stop() {
        if (this.serverSocket == null) return;
        this.executorService.shutdown();
        this.serverSocket.close();
    }

}
