package ru.t1.fpavlov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.fpavlov.tm.dto.request.ServerAboutRequest;
import ru.t1.fpavlov.tm.dto.request.ServerVersionRequest;
import ru.t1.fpavlov.tm.dto.response.ServerAboutResponse;
import ru.t1.fpavlov.tm.dto.response.ServerVersionResponse;

/**
 * Created by fpavlov on 17.03.2022.
 */
public interface ISystemEndpoint {

    @NotNull ServerAboutResponse getAbout(final ServerAboutRequest request);

    @NotNull ServerVersionResponse getVersion(final ServerVersionRequest request);

}
