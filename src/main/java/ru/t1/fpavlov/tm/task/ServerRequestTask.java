package ru.t1.fpavlov.tm.task;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.component.Server;
import ru.t1.fpavlov.tm.dto.request.AbstractRequest;

import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.Socket;

/**
 * Created by fpavlov on 01.04.2022.
 */
public final class ServerRequestTask extends AbstractServerSocketTask {

    public ServerRequestTask(@NotNull final Server server, @NotNull final Socket socket) {
        super(server, socket);
    }

    @Override
    @SneakyThrows
    public void run() {
        @NotNull final InputStream inputStream = this.socket.getInputStream();
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);
        @NotNull final Object object = objectInputStream.readObject();
        @NotNull final AbstractRequest request = (AbstractRequest) object;
        @Nullable Object responce = this.server.call(request);
        @NotNull final OutputStream outputStream = this.socket.getOutputStream();
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);
        objectOutputStream.writeObject(responce);
        this.server.submit(new ServerRequestTask(this.server, this.socket));
    }

}
