package ru.t1.fpavlov.tm.component;

import org.jetbrains.annotations.NotNull;
import ru.t1.fpavlov.tm.api.endpoint.Operation;
import ru.t1.fpavlov.tm.dto.request.AbstractRequest;
import ru.t1.fpavlov.tm.dto.response.AbstractResponse;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by fpavlov on 01.04.2022.
 */
public class Dispatcher {

    private final Map<Class<? extends AbstractRequest>, Operation<?, ?>> map = new LinkedHashMap<>();

    public <RQ extends AbstractRequest, RS extends AbstractResponse> void regitry(
            @NotNull final Class<RQ> reqClass, @NotNull final Operation<RQ, RS> operation
    ) {
        map.put(reqClass, operation);
    }

    public Object call(@NotNull final AbstractRequest request) {
        final Operation operation = map.get(request.getClass());
        return operation.execute(request);
    }

}
