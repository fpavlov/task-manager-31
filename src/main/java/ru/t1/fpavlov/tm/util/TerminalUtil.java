package ru.t1.fpavlov.tm.util;

import org.jetbrains.annotations.NotNull;

import java.util.Scanner;

/*
 * Created by fpavlov on 10.10.2021.
 */
public interface TerminalUtil {

    @NotNull
    Scanner SCANNER = new Scanner(System.in);

    @NotNull
    static String nextLine() {
        return SCANNER.nextLine();
    }

    @NotNull
    static Integer nextInteger() {
        @NotNull final String line;
        line = SCANNER.nextLine();
        return Integer.parseInt(line);
    }

}
