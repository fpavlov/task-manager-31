package ru.t1.fpavlov.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.fpavlov.tm.enumerated.Role;
import ru.t1.fpavlov.tm.model.User;

/**
 * Created by fpavlov on 19.12.2021.
 */
public interface IUserRepository extends IRepository<User> {

    @Nullable
    User create(@Nullable final String login, @Nullable final String password);

    @Nullable
    User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    );

    @NotNull
    User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    );

    @Nullable
    User findByLogin(@Nullable final String login);

    @Nullable
    User findByEmail(@Nullable final String email);

    @Nullable
    Boolean isLoginExist(@Nullable final String login);

    @Nullable
    Boolean isEmailExist(@Nullable final String email);

}
