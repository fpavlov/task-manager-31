package ru.t1.fpavlov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Created by fpavlov on 09.12.2021.
 */
public final class ApplicationQuiteCommand extends AbstractSystemCommand {

    @NotNull
    public static final String ARGUMENT = null;

    @NotNull
    public static final String DESCRIPTION = "Quite";

    @NotNull
    public static final String NAME = "exit";

    @Nullable
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.exit(0);
    }

}
