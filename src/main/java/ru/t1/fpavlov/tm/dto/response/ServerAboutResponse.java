package ru.t1.fpavlov.tm.dto.response;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by fpavlov on 17.03.2022.
 */
@Getter
@Setter
public class ServerAboutResponse extends AbstractResponse {

    private String email;

    private String name;

}
